
const burger = document.getElementById("burger");
const menu = document.getElementById("menu");

burger.addEventListener('click', () => {
    // Au clic sur le burger, afficher ou masquer le menu
    menu.classList.toggle('action');
});







                                /* Gestion de la Video Youtube*/


// Variable globale contenant l'etat du lecteur
let etatLecteur;

function lecteurPret(event) {
  // event.target = lecteur
  event.target.setVolume(50);
}

function changementLecteur(event) {
  // event.data = etat du lecteur
  etatLecteur = event.data;
}

let lecteur;

function onYouTubeIframeAPIReady() {
  lecteur = new YT.Player("video", {
    height: "390",
    width: "640",
    videoId: "n9xhJrPXop4",
    playerVars: {
      color: "white",
      enablejsapi: 1,
      modestbranding: 1,
      rel: 0
    },
    events: {
      onReady: lecteurPret,
      onStateChange: changementLecteur
    }
  });
}

// Hauteur de la video
const hauteurVideo = $("#video").height();
// Position Y de la video
const posYVideo = $("#video").offset().top;
// Valeur declenchant la modification de l'affichage (choix "esthetique")
const seuil = posYVideo + 0.75 * hauteurVideo;

// Gestion du defilement
$(".parallax").scroll(function () {
  // Recuperation de la valeur du defilement vertical
  const scroll = $(".parallax").scrollTop();

  // Classe permettant l'execution du CSS
  $("#video").toggleClass(
    "video",
    etatLecteur === YT.PlayerState.PLAYING && scroll > seuil
  );
});

// Fonction pour faire défiler le carrousel
function scrollCarrousel(direction) {
  const container = $(".carrousel-container");
  const items = $(".carrousel-item");
  const itemWidth = items.first().width();

  if (direction === "right") {
    container.animate({ scrollLeft: "+=" + itemWidth }, 500);
  } else {
    container.animate({ scrollLeft: "-=" + itemWidth }, 500);
  }
}

// Gestion des clics sur les flèches
$(".arrowleft").click(() => scrollCarrousel("left"));
$(".arrowright").click(() => scrollCarrousel("right"));
